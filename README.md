# foobic.github.io
My works. All layouts of websites are adaptive.

1. "Respect" - My first adaptive website layout <br>
Done: Only home page.<br>
Original: templatemonster #51576 <br>
Link to watch: [1_respect](https://foobic.github.io/1_respect) <br>

2. "Motion"(trimmed) - Very very simple landing page. <br>
PSD: Mahmoud Baghagho <br>
Link to watch: [2_motion](https://foobic.github.io/2_motion) <br>

3. "Notify theme" - simple landing page. <br>
PSD: Created by Michael Reimer from Best Psd Freebies<br>
Link to watch: [3_notify_theme](https://foobic.github.io/3_notify_theme) <br>
